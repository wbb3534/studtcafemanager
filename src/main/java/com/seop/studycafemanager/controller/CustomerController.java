package com.seop.studycafemanager.controller;

import com.seop.studycafemanager.model.CustomerItem;
import com.seop.studycafemanager.model.CustomerRequest;
import com.seop.studycafemanager.model.CustomerSeatNumberUpdateRequest;
import com.seop.studycafemanager.model.CustomerUseHourRequest;
import com.seop.studycafemanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);
        return "ok";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();
        return result;
    }
    @PutMapping("/seat-number/id/{id}")
    public String putCustomerSeatNumber(@PathVariable long id, @RequestBody @Valid CustomerSeatNumberUpdateRequest request) {
        customerService.putCustomerSeatNumber(id, request);
        return "ok";
    }

    @PutMapping("/use-hour/id/{id}")
    public String putCustomerUseHour(@PathVariable long id, @RequestBody @Valid CustomerUseHourRequest request) {
        customerService.putCustomerUseHour(id, request);
        return "ok";
    }

    @PutMapping("/leave/id/{id}")
    public String putCustomerLeaveTime(@PathVariable long id) {
        customerService.putCustomerLeaveTime(id);
        return "ok";
    }

    @DeleteMapping("/sign-out/id/{id}")
    public String delCustomer(@PathVariable long id) {
        customerService.delCustomer(id);
        return "ok";
    }
}
