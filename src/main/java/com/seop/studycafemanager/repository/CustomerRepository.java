package com.seop.studycafemanager.repository;

import com.seop.studycafemanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
