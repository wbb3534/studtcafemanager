package com.seop.studycafemanager.service;

import com.seop.studycafemanager.entity.Customer;
import com.seop.studycafemanager.model.CustomerItem;
import com.seop.studycafemanager.model.CustomerRequest;
import com.seop.studycafemanager.model.CustomerSeatNumberUpdateRequest;
import com.seop.studycafemanager.model.CustomerUseHourRequest;
import com.seop.studycafemanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addItem = new Customer();
        addItem.setCustomerName(request.getCustomerName());
        addItem.setCustomerPhone(request.getCustomerPhone());
        addItem.setSeatNumber(request.getSeatNumber());
        addItem.setUseHour(request.getUseHour());
        addItem.setUsedMember(request.getUsedMember());
        addItem.setUsedLocker(request.getUsedLocker());
        addItem.setTimeStart(LocalDateTime.now());
        addItem.setTimeEnd(LocalDateTime.now().plusHours(request.getUseHour()));

        customerRepository.save(addItem);
    }

    public List<CustomerItem> getCustomers() {
        List<Customer> origin = customerRepository.findAll();
        List<CustomerItem> result = new LinkedList<>();

        for (Customer item : origin) {
             CustomerItem addItem = new CustomerItem();
             addItem.setId(item.getId());
             addItem.setCustomerName(item.getCustomerName());
             addItem.setCustomerPhone(item.getCustomerPhone());
             addItem.setUseHour(item.getUseHour() + "시간");
             addItem.setSeatNumber(item.getSeatNumber() + "번");
             addItem.setTimeStart(item.getTimeStart());
             addItem.setTimeEnd(item.getTimeEnd());
             addItem.setLeaveTime(item.getLeaveTime());
             addItem.setUsedLocker(item.getUsedLocker());
             addItem.setUsedMember(item.getUsedMember());

             result.add(addItem);
        }
        return result;
    }

    public void putCustomerSeatNumber(long id, CustomerSeatNumberUpdateRequest request) {
        Customer origin = customerRepository.findById(id).orElseThrow();
        origin.setSeatNumber(request.getSeatNumber());

        customerRepository.save(origin);
    }

    public void putCustomerUseHour(long id, CustomerUseHourRequest request) {
        Customer origin = customerRepository.findById(id).orElseThrow();
        origin.setUseHour(request.getUseHour());
        origin.setTimeEnd(origin.getTimeStart().plusHours(request.getUseHour()));

        customerRepository.save(origin);
    }

    public void putCustomerLeaveTime(long id) {
        Customer origin = customerRepository.findById(id).orElseThrow();
        origin.setLeaveTime(LocalDateTime.now());

        customerRepository.save(origin);
    }

    public void delCustomer(long id) {
        customerRepository.deleteById(id);
    }
}
