package com.seop.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class CustomerSeatNumberUpdateRequest {
    @NotNull
    private Integer seatNumber;
}
