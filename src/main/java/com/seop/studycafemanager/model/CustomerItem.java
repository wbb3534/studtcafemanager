package com.seop.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {
    private Long id;
    private String customerName;
    private String customerPhone;
    private String seatNumber;
    private String useHour;
    private LocalDateTime timeStart;
    private LocalDateTime timeEnd;
    private LocalDateTime leaveTime;
    private Boolean UsedMember;
    private Boolean usedLocker;
}
