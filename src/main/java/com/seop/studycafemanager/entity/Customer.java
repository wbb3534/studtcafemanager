package com.seop.studycafemanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 20)
    private String customerPhone;
    @Column(nullable = false)
    private Integer seatNumber;
    @Column(nullable = false)
    private Integer useHour;
    @Column(nullable = false)
    private LocalDateTime timeStart;
    private LocalDateTime leaveTime;
    @Column(nullable = false)
    private LocalDateTime timeEnd;
    @Column(nullable = false)
    private Boolean UsedMember;
    @Column(nullable = false)
    private Boolean UsedLocker;
}
